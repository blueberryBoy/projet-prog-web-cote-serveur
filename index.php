
<?php
require_once("clicksCount.php");
require_once("API.php");
$api = new API();

//Renvoie les coordonnnees d'un etablissement (precise par son identifiant unique, son uai)
function getCoordinates($uai) {
  $api = new API();
  $records = $api->etablissement_request($uai);
  foreach ($records as $key => $record) {
    return $record["fields"]["coordonnees"];
  }
}

//Renvoie l'URL d'un etablissement (precise par son identifiant unique, son uai)
function getURL($uai) {
  $api = new API();
  $records = $api->etablissement_request($uai);
  foreach ($records as $key => $record) {
    return $record["fields"]["url"];
  }
}

//Renvoie true si les deux facets sont egaux, sert au tri des facets pour l'affichage des filtres
function compare_facets($facet1, $facet2) {
  return strcmp($facet1["name"], $facet2["name"]);
}

/*On recupere en ligne grace aux url les informations nous
servant a afficher les differents options des menus deroulants (les filtres)
dans les tableaux regions, diplomes et domaines*/

$regions = $api->facet_request("reg_etab_lib");
$diplomes = $api->facet_request("diplome_rgp");
$domaines = $api->facet_request("sect_disciplinaire_lib");

if (isset($_POST['submitBtn'])) {
  $keyWords = $_POST['keyWords'];
  $filiere = $_POST['discipline'];
  $type_diplome = $_POST['type_diplome'];
  $region = $_POST['region'];

  $records = $api->extraCriterias_request($filiere, $type_diplome, $region, $keyWords);
  $recordsNb = count($records);
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156845178-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156845178-1');
        </script>

        <script>
        function sendData(uai, name) {
          $.ajax({
                url: 'clicksCount.php',
                type: 'POST',
                data: 'uai=' + uai + '&name=' + name,
            });
          /* Sert a faire un affichage test dans une textarea pour verifier le bon fonctionnement de cette fonction
          var oBtn = document.getElementById("test");
          if(oBtn){
              oBtn.innerHTML = uai;
          }*/
        }
        function openPage(url) {
          window.open(url, "_blank");
        }
        </script>

        <title>Recherche de formations dans le superieur</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="assets/jQuery/jquery-ui.css">
        <link rel="stylesheet" href="assets/leaflet/leaflet.css"/>
        <script src="assets/jQuery/jquery-3.4.1.js"></script>
        <script src="assets/jQuery/jquery-ui.js"></script>
        <script src="assets/leaflet/leaflet.js"></script>

        <!--<script>
      $( function() {
        $( "#tabs" ).tabs();
      } );
    </script>-->
    </head>
    <body>
      <div class="linkContainer">
        <div class="link">
          <a class="gitLink" href="https://bitbucket.org/blueberryBoy/projet-prog-web-cote-serveur/src/master/">Depot de code</a>
        </div>
      </div>

      <form name ="submitForm" method ="POST" action = "index.php">
      <div class="searchForm">
           <input name="keyWords" type="search" value="" id="searchBar"  class="searchElem">
           <input type="submit" value="Rechercher" id="submitButton" name="submitBtn"  class="searchElem">
      </div>
      <div class="extraCriterias">
         <select name="discipline" class="extraCritera">
          <option value="all">Toutes les disciplines</option>
         <?php
         usort($domaines, compare_facets);
         for ($i = 0;$i<count($domaines);$i++) {
           //On met comme option du select les differentes filieres recuperees precedemment
           echo "<option value=\"".$domaines[$i]["name"]."\">".$domaines[$i]["name"]."</option>";
         }
         ?>
         </select>

         <select name="type_diplome" class="extraCritera">
          <option value="all">Tous les diplomes</option>
          <?php
          usort($diplomes, compare_facets);
          for ($i = 0;$i<count($diplomes);$i++) {
            //Meme traitement pour les differents types de diplome
            echo "<option value=\"".$diplomes[$i]["name"]."\">".$diplomes[$i]["name"]."</option>";
          }
          ?>
        </select>

        <select name="region" class="extraCritera">
        <option value="all">Toutes les regions</option>
        <?php
        usort($regions, compare_facets);
        for ($i = 0;$i<count($regions);$i++) {
          //Et toujours le meme traitement pour les regions
          echo "<option value=\"".$regions[$i]["name"]."\">".$regions[$i]["name"]."</option>";
        }
        ?>
       </select>
      </div>
      </form>

      <!--Ce textarea sert de zone d'affichage test pour le code ajax servant a
      compter le nombre de clics sur les liens d'etablissement
      <textarea id='test'>
        Blabla
      </textarea>
      -->

      <div id="bottomContainer">
            <div id="searchResults">
              <ul>
                <?php
                if (isset($_POST['submitBtn'])) {

                  if ($searchStrJsonFileContents !== false) {
                    echo "<b>Il y a ".$recordsNb." resultats a votre recherche.</b><br>";
                    $markers = array();
                    foreach ($records as $key => $record) {
                      $type_diplome = $record["fields"]["diplome_rgp"];
                      $nom_formation = $record["fields"]["libelle_intitule_1"];
                      $nom_etab = $record["fields"]["etablissement_lib"];
                      $ville_etab = $record["fields"]["com_etab_lib"];
                      $region_etab = $record["fields"]["reg_etab_lib"];

                      //On recupere grace a l'identifiant de l'etablissement (uai) dans une autre OpenDB les coordonnees et l'url de l'etablissement
                      $uai = $record["fields"]["etablissement"];
                      $coordinates = getCoordinates($uai);
                      $url = getURL($uai);
                      $infos_formation = $type_diplome." ".$nom_formation." - ".$nom_etab."<br>Ville : ".$ville_etab." (".$region_etab.")<br> <a href=\"".$url."\">Site web de l'établissement</a>";

                      $nb_clics = getNbClicks($uai);
                      //$nb_clics = 0;

                      //On cree un tableau contenant les coordonnees et les informations sur la formation ainsi que le lien pour le site de l'etablissement (afin de les mettre dans le popup du marqueur)
                      $marker = array($coordinates, $nom_etab." (".$region_etab.")".'<br>Nombre de clics : '.$nb_clics.'<br><button type="button" onclick="openPage(`'.$url. '`); sendData(`'.$uai.'`,`'.$nom_etab.'`)">Site</button>');
                      //On ajoute ce tableau representant un marqueur au tableau contenant tous les marqueurs pour les ajouter ensuite a la carte
                      array_push($markers, $marker);

                      echo "<br><li>".$infos_formation."</li><br>";
                    }
                  }

                }
                else {
                  echo "<li>Vous n'avez pas encore fait de recherche !</li>";
                }
                ?>
              </ul>
          </div>
            <!--Code servant a creer et afficher la carte et les marqueurs-->
            <div id="mapcontainer">
              <script>
               <?php
               //Si l'utilisateur a deja fait une recherche, on centre la carte sur les coordonnees du premier resultat
               if (isset($_POST['submitBtn']) && $markers[0][0] != NULL) {
                 echo "var mymap = L.map('mapcontainer').setView(".json_encode($markers[0][0]).", 13);";
               } else { //Sinon on centre la carte sur l'universite Paris Sorbonne
                 echo "var mymap = L.map('mapcontainer').setView([48.850754,2.3412856], 13);";
               }
                ?>

               L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 18,
              id: 'mapbox/streets-v11',
              accessToken: 'pk.eyJ1IjoiZmFlcnJpbmciLCJhIjoiY2s0ZTcybmZnMDl6MzNsb2I0dTQ0c2xweCJ9.gLyRWBfiicTrXuhLYm9pZw'
              }).addTo(mymap);
              //On cree une icone personnalisee pour notre carte
              var myIcon = L.icon({
               iconUrl: 'assets/images/educationIcon.png',
               iconSize: [48, 48],
               popupAnchor: [0, -20]
             });

              <?php
              if (isset($_POST['submitBtn'])) {
                foreach ($markers as $key => $marker) {
                  //On recupere les coordonnees et la description pour chaque marqueur afin de l'ajouter a la carte avec son popup
                  echo "var coordinates = ".json_encode($marker[0]).";";
                  echo "var description = ".json_encode($marker[1]).";";
                  if ($marker[0] != NULL) {
                    echo "var marker = L.marker(coordinates,{icon: myIcon}).addTo(mymap);";
                    echo "marker.bindPopup(description).openPopup();";
                  }
                }

              }
              ?>
              </script>
            </div>
      <div id="clear"></div>
   </div>

    </body>
</html>
