<?php
  class API {

    private $url_formations = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics";
    private $url_etablissements = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur";
    private $api_key = "a68c7454bc2f1e5e5726b950dfd664616b3b86b30dc5621e1fd1575f";
    private $rows_nb = "20";

    public function facet_request($facet_name) {
      $url = $this->url_formations."&apikey=".$this->api_key."&rows=0&facet=".$facet_name;
      $strJsonFileContents = file_get_contents($url, true);
      if ($strJsonFileContents === false) {
        echo("Erreur : Impossible d'utiliser l'API permettant d'effectuer la recherche a l'heure actuelle !");

      } else {
        $json = json_decode($strJsonFileContents, true);
        return $json["facet_groups"][0]["facets"];
      }
    }

    public function etablissement_request($uai) {
      $url = $this->url_etablissements."&apikey=".$this->api_key."&rows=".$this->rows_nb."&refine.uai=".$uai;
      $searchStrJsonFileContents = file_get_contents($url, true);
      if ($searchStrJsonFileContents === false) {
        echo("Erreur : Impossible d'utiliser l'API permettant d'effectuer la recherche a l'heure actuelle !");

      } else {
        $json_search = json_decode($searchStrJsonFileContents, true);
        return $json_search["records"];
      }
    }

    public function formation_request($field_name, $field_value) {
      $url = $this->url_formations."&apikey=".$this->api_key."&rows=".$this->rows_nb."&refine.rentree_lib=2017-18&refine.".$field_name."=".$field_value;
      $searchStrJsonFileContents = file_get_contents($url, true);
      if ($searchStrJsonFileContents === false) {
        echo("Erreur : Impossible d'utiliser l'API permettant d'effectuer la recherche a l'heure actuelle !");

      } else {
        $json_search = json_decode($searchStrJsonFileContents, true);
        return $json_search["records"];
      }
    }

    public function extraCriterias_request($filiere, $type_diplome, $region, $keyWords) {
      $searchURL = $this->url_formations."&rows=".$this->rows_nb."&refine.rentree_lib=2017-18";
      //On ajoute a l"URL les differents filtres si ils ont ete changes de leur valeur par defaut
      if ($filiere != "all") {
        $searchURL .= "&refine.sect_disciplinaire_lib=".$filiere;
      }
      if ($type_diplome != "all") {
        $searchURL .= "&refine.diplome_rgp=".$type_diplome;
      }
      if ($region != "all") {
        $searchURL .= "&refine.reg_etab_lib=".$region;
      }
      if (empty($keyWords) == false) {
        //On ajoute a l'url les differents mots cles tapes dans la barre de recherche simplement en remplacant l'espace les separant par un +
        $searchURL .= "&q=".str_replace(" ", "+", $keyWords);
      }

      //On effectue la recherche
      $searchStrJsonFileContents = file_get_contents($searchURL, true);
      if ($searchStrJsonFileContents === false) {
        echo("Erreur : Impossible d'utiliser l'API permettant d'effectuer la recherche a l'heure actuelle !");

      } else {
        $json_search = json_decode($searchStrJsonFileContents, true);
        return $json_search["records"];
    }
  }
}
?>
